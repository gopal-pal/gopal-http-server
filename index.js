const http = require("http");
const fs = require("fs");
const uuid = require("uuid4");
const path = require("path");

const server = http.createServer((req, res) => {
  if (req.method === "GET") {
    if (req.url === "/html") {
      fs.readFile("./index.html", "utf8", (err, htmlData) => {
        if (err) {
          res.writeHead(500);
          res.write("Something went wrong");
        } else {
          res.writeHead(200, {
            "Content-Type": "text/html",
          });
          res.write(htmlData);
        }
        res.end();
      });
    } else if (req.url === "/json") {
      fs.readFile("./data.json", "utf8", (err, jsonData) => {
        if (err) {
          res.writeHead(500);
          res.write("Something went wrong");
        } else {
          res.writeHead(200, {
            "Content-Type": "application/JSON",
          });
          res.write(jsonData);
        }
        res.end();
      });
    } else if (req.url === "/uuid") {
      res.writeHead(200, {
        "Content-Type": "application/JSON",
      });
      let uuidObject = { name: uuid() };
      res.write(JSON.stringify(uuidObject));
      res.end();
    } else if (req.url.includes("/status")) {
      let statusCode = path.basename(req.url);
      let responseStatus = http.STATUS_CODES[statusCode];
      if (responseStatus) {
        res.writeHead(parseInt(statusCode));
        res.write(responseStatus);
        res.end();
      } else if (statusCode == "status") {
        res.writeHead(400);
        res.write(http.STATUS_CODES[400]);
        res.end();
      } else {
        res.writeHead(404);
        res.write(http.STATUS_CODES[404]);
        res.end();
      }
    } else if (req.url.includes("/delay")) {
      let delayTime = parseInt(path.basename(req.url));
      if (isNaN(delayTime)) {
        res.writeHead(400);
        res.write(http.STATUS_CODES[400]);
        res.end();
      } else {
        setTimeout(() => {
          res.writeHead(200);
          res.write(http.STATUS_CODES[200]);
          res.end();
        }, delayTime * 1000);
      }
    } else {
      res.writeHead(404);
      res.write(http.STATUS_CODES[404]);
      res.end();
    }
  } else {
    res.writeHead(405);
    res.write(http.STATUS_CODES[405]);
    res.end();
  }
});

server.listen(8080, () => {
  console.log("Server is Running...");
});
